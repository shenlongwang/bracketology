import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from subprocess import check_output
# import matplotlib.pyplot as plt
import tensorflow as tf
from models import *
from utils import *

  
print(check_output(["ls", "./data"]).decode("utf8"))
train_data, team_dict = read_data("./data/RegularSeasonDetailedResults.csv", 2015)
test_data, team_dict = read_data("./data/TourneyDetailedResults.csv", 2015, team_dict)
train = train_data.values
# test_data.head()
test = test_data.values

params = {}
params["n_team"] = team_dict.shape[0]
params["batch_size"] = 65535 
params["lr"] = 1e-3
params["gamma"] = 1e-4 
params["sigma"] = 0.5 
params["n_epoch"] = 7000 
params["rank"] = 5
params["gamma"] = 1e-2
params["print"] = False 

highest_acc = 0.0
highest_params = params.copy() 


# hyper-parameter grid search
# for params["rank"] in (5, 15, 30): 
for params["lr"] in (5e-3, 1e-3): 
    for params["n_epoch"] in(7000, 15000, 24000): 
    # for params["gamma"] in (1e-2, 1e-3): 
        mf = BiasedMatrixFactorization(params)
        mf.train(train[:, 0], train[:, 1], train[:, 3])
        batch_acc, batch_loss = mf.validation(test[:, 0], test[:, 1], test[:, 3])
        if batch_acc > highest_acc:
            highest_acc = batch_acc
            highest_params = params.copy()
        mf.sess.close()
        del mf
print(highest_params["lr"], highest_params["n_epoch"], highest_acc)
# with tf.Session() as sess:
#     onehot_vector = sess.run(test_out.team1_onehot, feed_dict={test_out.team1: train[:, 0]})
#     print(onehot_vector[0])
#     print(train[0, 0])
#     print(onehot_vector[0][train[0, 0]])
#     print(onehot_vector.shape)
