
# OLD_PYTHONPATH="$PYTHONPATH"
# OLD_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
# OLD_PATH="$PATH"
# export PYTHONPATH=/pkgs/tensorflow-gpu-cuda8-11Oct2016:/usr/lib/python2.7/dist-packages:$PYTHONPATH
# export LD_LIBRARY_PATH=/pkgs/cuda-8.0/lib64:/pkgs/cudnn-7.5:$LD_LIBRARY_PATH
# export PATH=/usr/bin:/local/bin:/usr/local/bin:/bin:$GUROBI_HOME/bin:$CUDA_HOME/bin:$CUDA_HOME/lib64:/usr/lib/i386-linux-gnu:/opt/ros/jade/bin/:/u/slwang/.local/bin/
# echo 'PYTHONPATH set to ' $PYTHONPATH
# echo 'LD_LIBRARY_PATH set to ' $LD_LIBRARY_PATH
# echo 'Sending job ' $1
# python alexnet_original.py
# python /ais/gobi4/slwang/tflearn/setup.py install --user 
source activate py34
# srun --gres=gpu:1 -c 2 -l -p gpuc python demo_withloc.py
srun --gres=gpu:1 -c 2 -l -p gpuc python demo_final.py
source deactivate
# srun --gres=gpu:1 -c 2 -l -p gpuc python $1 &
# export PYTHONPATH=$OLD_PYTHONPATH
# export PATH=$PATH
# export LD_LIBRARY_PATH=$OLD_LD_LIBRARY_PATH
