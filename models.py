import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import numpy as np # linear algebra
from subprocess import check_output
import tensorflow as tf


class LocMatrixFactorization:
    def __init__(self, params):
        self.n_team = params["n_team"]
        self.rank = params["rank"]
        self.gamma = params["gamma"]
        self.sigma = params["sigma"]
        self.lr = params["lr"]
        self.n_epoch = params["n_epoch"]
        self.batch_size = params["batch_size"]
        tf.reset_default_graph()
        self.comp_graph()
        self.sess = tf.Session()
    
    def train(self, x1, x2, loc1, loc2, d, y):
        n_sample = len(y)
        b = self.batch_size
        self.sess.run(self.init_op) # initialization
        for n in range(self.n_epoch):
            rand_idx = np.random.permutation(n_sample)
            print('Start training...Epoch #%d' % n)
            for i in range(0, n_sample, b):
                batch_x1 = x1[rand_idx[i:i+b]]
                batch_x2 = x2[rand_idx[i:i+b]]
                batch_loc1 = loc1[rand_idx[i:i+b]]
                batch_loc2 = loc2[rand_idx[i:i+b]]
                batch_d = d[rand_idx[i:i+b]]
                batch_y = y[rand_idx[i:i+b]]
                # print(batch_x1.shape, batch_x2.shape, batch_y.shape)
                feed_dict = {self.team1: batch_x1, self.team2: batch_x2, self.score: batch_y, self.loc1: batch_loc1, self.loc2: batch_loc2, self.day: batch_d}
                _, prediction, batch_acc, batch_loss = self.sess.run([self.opt, self.pred, self.accuracy, self.loss], feed_dict)
                if (i % 1 == 0):
                    print('Epoch #%d, Step #%d: loss %2.4f, acc %2.4f, y %f, pred %f' % (n, i, batch_loss, batch_acc, batch_y[0], prediction[0])) 
    
    def validation(self, x1, x2, loc1, loc2, d, y):
        n_sample = len(y)
        b = self.batch_size
        print('Start validation...')
        acc = []
        for i in range(0, n_sample, b):
            batch_x1 = x1[i:i+b]
            batch_x2 = x2[i:i+b]
            batch_loc1 = loc1[i:i+b]
            batch_loc2 = loc2[i:i+b]
            batch_d = d[i:i+b]
            batch_y = y[i:i+b]
            # feed_dict = {self.team1: batch_x1, self.team2: batch_x2, self.score: batch_y}
            feed_dict = {self.team1: batch_x1, self.team2: batch_x2, self.score: batch_y, self.loc1: batch_loc1, self.loc2: batch_loc2, self.day: batch_d}
            prediction, batch_acc, batch_loss = self.sess.run([self.pred, self.accuracy, self.loss], feed_dict)
            print('Average loss %2.4f, average acc %2.4f, y %f, pred %f' % (batch_loss, batch_acc, batch_y[0], prediction[0])) 
        return batch_acc, batch_loss 

    def predict(self, x1, x2, loc1, loc2):
        prediction = self.sess.run([self.pred], feed_dict={self.team1: x1, self.team2: x2, self.loc1: loc1, self.loc2: loc2})        
        return prediction
    
    def comp_graph(self):
        self.team1 = tf.placeholder(shape=[None], dtype=tf.int32, name = "team1_idx")
        self.team2 = tf.placeholder(shape=[None], dtype=tf.int32, name = "team2_idx")
        self.loc1 = tf.placeholder(shape=[None], dtype=tf.float32, name = "loc1")
        self.loc2 = tf.placeholder(shape=[None], dtype=tf.float32, name = "loc2")
        self.score = tf.placeholder(shape=[None], dtype=tf.float32, name = "target_score")
        self.day = tf.placeholder(shape=[None], dtype=tf.float32, name = "time")
        self.W1 = tf.get_variable("weight1", shape=[self.n_team, self.rank], initializer=tf.truncated_normal_initializer(stddev=0.02))
        self.W2 = tf.get_variable("weight2", shape=[self.n_team, self.rank], initializer=tf.truncated_normal_initializer(stddev=0.02))
        self.b_global = tf.get_variable("bias", shape=[])
        self.b_team1 = tf.get_variable("bias_team1", shape=[self.n_team])
        self.b_team2 = tf.get_variable("bias_team2", shape=[self.n_team])
        self.b_loc1 = tf.get_variable("bias_loc1", shape=[self.n_team])
        self.b_loc2 = tf.get_variable("bias_loc2", shape=[self.n_team])
        self.f1 = tf.nn.embedding_lookup(self.W1, self.team1, name = "fea1")
        self.f2 = tf.nn.embedding_lookup(self.W2, self.team2, name = "fea2")
        self.b1 = tf.nn.embedding_lookup(self.b_team1, self.team1, name = "fea1")
        self.b2 = tf.nn.embedding_lookup(self.b_team2, self.team2, name = "fea2")
        self.bl1 = tf.nn.embedding_lookup(self.b_loc1, self.team1, name = "fea1") * self.loc1
        self.bl2 = tf.nn.embedding_lookup(self.b_loc2, self.team2, name = "fea2") * self.loc2
        self.pred = tf.add(tf.reduce_sum(tf.multiply(self.f1, self.f2), reduction_indices = 1), self.b_global) 
        self.pred = tf.add(self.pred, self.b1+self.bl1)
        self.pred = tf.add(self.pred, self.b2+self.bl2)
        self.regularization = tf.nn.l2_loss(self.W1, name = "prior1") + tf.nn.l2_loss(self.W2, name = "prior2")
        self.loss = tf.nn.l2_loss(self.pred - self.score, name = "data_term") + self.gamma * self.regularization
        self.accuracy = tf.reduce_mean(tf.to_float(tf.equal(tf.sign(self.pred), tf.sign(self.score))), name = "accuracy")
        self.opt = tf.train.AdamOptimizer(self.lr).minimize(self.loss)
        self.init_op = tf.global_variables_initializer()

class FinalMatrixFactorization:
    def __init__(self, params):
        self.n_team = params["n_team"]
        self.rank = params["rank"]
        self.gamma = params["gamma"]
        self.sigma = params["sigma"]
        self.lr = params["lr"]
        self.n_epoch = params["n_epoch"]
        self.batch_size = params["batch_size"]
        tf.reset_default_graph()
        self.comp_graph()
        self.sess = tf.Session()
    
    def train(self, x1, x2, loc1, loc2, d, yo, yd):
        n_sample = len(yo)
        b = self.batch_size
        self.sess.run(self.init_op) # initialization
        for n in range(self.n_epoch):
            rand_idx = np.random.permutation(n_sample)
            print('Start training...Epoch #%d' % n)
            for i in range(0, n_sample, b):
                batch_x1 = x1[rand_idx[i:i+b]]
                batch_x2 = x2[rand_idx[i:i+b]]
                batch_loc1 = loc1[rand_idx[i:i+b]]
                batch_loc2 = loc2[rand_idx[i:i+b]]
                batch_d = d[rand_idx[i:i+b]]
                batch_yo = yo[rand_idx[i:i+b]]
                batch_yd = yd[rand_idx[i:i+b]]
                # print(batch_x1.shape, batch_x2.shape, batch_y.shape)
                feed_dict = {self.team1: batch_x1, self.team2: batch_x2, self.oscore: batch_yo, self.dscore: batch_yd, self.loc1: batch_loc1, self.loc2: batch_loc2, self.day: batch_d}
                _, prediction_o, prediction_d, batch_acc, batch_loss = self.sess.run([self.opt, self.predO, self.predD, self.accuracy, self.loss], feed_dict)
                if (i % 1 == 0):
                    print('Epoch #%d, Step #%d: loss %2.4f, acc %2.4f, yo %f, predo %f, yd %f, predd %f' % (n, i, batch_loss, batch_acc, batch_yo[0], prediction_o[0], batch_yd[0], prediction_d[0])) 
    
    def validation(self, x1, x2, loc1, loc2, d, yo, yd):
        n_sample = len(yo)
        b = self.batch_size
        print('Start validation...')
        acc = []
        for i in range(0, n_sample, b):
            batch_x1 = x1[i:i+b]
            batch_x2 = x2[i:i+b]
            batch_loc1 = loc1[i:i+b]
            batch_loc2 = loc2[i:i+b]
            batch_d = d[i:i+b]
            batch_yo = yo[i:i+b]
            batch_yd = yd[i:i+b]
            # feed_dict = {self.team1: batch_x1, self.team2: batch_x2, self.score: batch_y}
            feed_dict = {self.team1: batch_x1, self.team2: batch_x2, self.oscore: batch_yo, self.dscore: batch_yd, self.loc1: batch_loc1, self.loc2: batch_loc2, self.day: batch_d}
            prediction, batch_acc, batch_loss = self.sess.run([self.predO, self.accuracy, self.loss], feed_dict)
            prediction_o, prediction_d, batch_acc, batch_loss = self.sess.run([self.predO, self.predD, self.accuracy, self.loss], feed_dict)
            print('Average loss %2.4f, acc %2.4f, yo %f, predo %f, yd %f, predd %f' % (batch_loss, batch_acc, batch_yo[0], prediction_o[0], batch_yd[0], prediction_d[0])) 
        return batch_acc, batch_loss 

    def predict(self, x1, x2, loc1, loc2, d, yo, yd):
        feed_dict = {self.team1: x1, self.team2: x2, self.oscore: yo, self.dscore: yd, self.loc1: loc1, self.loc2: loc2, self.day: d}
        prediction_o, prediction_d, _, _ = self.sess.run([self.predO, self.predD, self.accuracy, self.loss], feed_dict)
        return prediction_o, prediction_d
    
    def comp_graph(self):
        self.team1 = tf.placeholder(shape=[None], dtype=tf.int32, name = "team1_idx")
        self.team2 = tf.placeholder(shape=[None], dtype=tf.int32, name = "team2_idx")
        self.oscore = tf.placeholder(shape=[None], dtype=tf.float32, name = "offense_score")
        self.dscore = tf.placeholder(shape=[None], dtype=tf.float32, name = "defense_score")
        self.day = tf.placeholder(shape=[None], dtype=tf.float32, name = "time")
        self.loc1 = tf.placeholder(shape=[None], dtype=tf.float32, name = "loc1")
        self.loc2 = tf.placeholder(shape=[None], dtype=tf.float32, name = "loc2")
        self.WO = tf.get_variable("weight_offense", shape=[self.n_team, self.rank], initializer=tf.truncated_normal_initializer(stddev=0.02))
        self.WD = tf.get_variable("weight_defense", shape=[self.n_team, self.rank], initializer=tf.truncated_normal_initializer(stddev=0.02))
        self.b_global = tf.get_variable("bias_global", shape=[])
        self.b_teamo = tf.get_variable("bias_teamo", shape=[self.n_team])
        self.b_teamd = tf.get_variable("bias_teamd", shape=[self.n_team])
        self.b_loc1 = tf.get_variable("bias_loc1", shape=[self.n_team])
        self.b_loc2 = tf.get_variable("bias_loc2", shape=[self.n_team])
        self.bl1 = tf.nn.embedding_lookup(self.b_loc1, self.team1, name = "fea1") * self.loc1
        self.bl2 = tf.nn.embedding_lookup(self.b_loc2, self.team2, name = "fea2") * self.loc2
        self.f1O = tf.nn.embedding_lookup(self.WO, self.team1, name = "fea1_off")
        self.f2O = tf.nn.embedding_lookup(self.WO, self.team2, name = "fea2_off")
        self.f1D = tf.nn.embedding_lookup(self.WD, self.team1, name = "fea1_def")
        self.f2D = tf.nn.embedding_lookup(self.WD, self.team2, name = "fea2_def")
        self.b1O = tf.nn.embedding_lookup(self.b_teamo, self.team1, name = "b1_off")
        self.b2O = tf.nn.embedding_lookup(self.b_teamo, self.team2, name = "b2_off")
        self.b1D = tf.nn.embedding_lookup(self.b_teamd, self.team1, name = "b1_def")
        self.b2D = tf.nn.embedding_lookup(self.b_teamd, self.team2, name = "b2_def")
        self.predO = tf.add(tf.reduce_sum(tf.multiply(self.f1O, self.f2D), reduction_indices = 1), self.b_global) 
        self.predO = tf.add(self.predO, self.b1O+self.bl1-self.bl2) 
        self.predO = tf.add(self.predO, self.b2D) 
        self.predD = tf.add(tf.reduce_sum(tf.multiply(self.f1D, self.f2O), reduction_indices = 1), self.b_global) 
        self.predD = tf.add(self.predD, self.b1D+self.bl2-self.bl1) 
        self.predD = tf.add(self.predD, self.b2O) 
        self.regularization = tf.nn.l2_loss(self.WO, name = "prior1") + tf.nn.l2_loss(self.WD, name = "prior2")
        self.lossD = tf.nn.l2_loss(self.predD - self.dscore, name = "d_term") 
        self.lossO = tf.nn.l2_loss(self.predO - self.oscore, name = "o_term") 
        self.loss =  self.lossO + self.lossD + self.gamma * self.regularization
        self.accuracy = tf.reduce_mean(tf.to_float(tf.equal(tf.sign(self.predO - self.predD), tf.sign(self.oscore - self.dscore))), name = "accuracy")
        self.opt = tf.train.AdamOptimizer(self.lr).minimize(self.loss)
        self.init_op = tf.global_variables_initializer()
