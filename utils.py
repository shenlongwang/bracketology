import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import numpy as np

def read_data(filename, year = None, team_dict = None):
    dr = pd.read_csv(filename)
    if year != None:
        dr = dr[dr["Season"] == year]
    data_frame_1 = pd.DataFrame()
    data_frame_1[["team1", "team2"]] =dr[["Wteam", "Lteam"]].copy()
    data_frame_1["class"] = 1
    data_frame_1["score"] = dr["Wscore"] - dr["Lscore"]
    data_frame_1["day"] = dr["Daynum"]
    data_frame_1["loc1"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'H').astype('float') * 1.0
    data_frame_1["loc2"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'A').astype('float') * 1.0
    data_frame_2 = pd.DataFrame()
    data_frame_2[["team1", "team2"]] =dr[["Lteam", "Wteam"]]
    data_frame_2["class"] = 0
    data_frame_2["score"] = dr["Lscore"] - dr["Wscore"] 
    data_frame_2["day"] = dr["Daynum"]
    data_frame_2["loc1"] = 1.0 - data_frame_1["loc1"].copy()
    data_frame_2["loc2"] = 1.0 - data_frame_1["loc2"].copy()
    # data_frame_1["loc1"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'A').astype('float') * 1.0
    # data_frame_1["loc2"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'H').astype('float') * 1.0
    data_frame = pd.concat((data_frame_1, data_frame_2), axis=0)
    if team_dict is None:
        team_dict = data_frame.team1.unique()
    trans_dict = {t: i for i, t in enumerate(team_dict)}
    data_frame["team1"] = data_frame["team1"].apply(lambda x: trans_dict[x])
    data_frame["team2"] = data_frame["team2"].apply(lambda x: trans_dict[x])
    print(data_frame.head())
    return data_frame, team_dict

def read_test(filename, team_dict):
    dr = pd.read_csv(filename)
    for i in dr:
        print(i)
    data_frame = pd.DataFrame()
    data_frame["team1"] = dr["Id"].apply(lambda x: x.split('_')[1]) 
    data_frame["team2"] = dr["Id"].apply(lambda x: x.split('_')[2]) 
    trans_dict = {t: i for i, t in enumerate(team_dict)}
    data_frame["team1"] = data_frame["team1"].apply(lambda x: trans_dict[int(x)])
    data_frame["team2"] = data_frame["team2"].apply(lambda x: trans_dict[int(x)])
    data_frame["year"] = 2
    data_frame["Oscore"] = 0 
    data_frame["Dscore"] = 0 
    data_frame["day"] = 150.0 + 400.0 
    data_frame["loc1"] = 0.5 
    data_frame["loc2"] = 0.5 
    return data_frame 

def read_data_od(filename, year = None, team_dict = None):
    dr = pd.read_csv(filename)
    if year != None:
        dr = dr[dr["Season"].isin(year)]
    data_frame_1 = pd.DataFrame()
    data_frame_1[["team1", "team2"]] =dr[["Wteam", "Lteam"]].copy()
    data_frame_1["year"] = dr["Season"] - year[0] 
    data_frame_1["Oscore"] = dr["Wscore"]
    data_frame_1["Dscore"] = dr["Lscore"]
    data_frame_1["day"] = dr["Daynum"].astype('float') + (data_frame_1["year"].astype('float'))*200.0
    data_frame_1["loc1"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'H').astype('float') * 1.0
    data_frame_1["loc2"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'A').astype('float') * 1.0
    data_frame_2 = pd.DataFrame()
    data_frame_2[["team1", "team2"]] =dr[["Lteam", "Wteam"]]
    data_frame_2["year"] = dr["Season"] - year[0] 
    data_frame_2["Oscore"] = dr["Lscore"]
    data_frame_2["Dscore"] = dr["Wscore"]
    data_frame_2["day"] = dr["Daynum"].astype('float') + (data_frame_2["year"].astype('float'))*200.0
    data_frame_2["loc1"] = 1.0 - data_frame_1["loc1"].copy()
    data_frame_2["loc2"] = 1.0 - data_frame_1["loc2"].copy()
    # data_frame_1["loc1"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'A').astype('float') * 1.0
    # data_frame_1["loc2"] = (dr["Wloc"] == 'N').astype('float')*0.5 + (dr["Wloc"] == 'H').astype('float') * 1.0
    data_frame = pd.concat((data_frame_1, data_frame_2), axis=0)
    if team_dict is None:
        team_dict = data_frame.team1.unique()
    trans_dict = {t: i for i, t in enumerate(team_dict)}
    data_frame["team1"] = data_frame["team1"].apply(lambda x: trans_dict[x])
    data_frame["team2"] = data_frame["team2"].apply(lambda x: trans_dict[x])
    print(data_frame.head())
    return data_frame, team_dict

def pack_result(team1, team2, oscore, dscore, team_dict, out_file):
    prob = 1 / (1 + np.exp((oscore-dscore) / 10.0)) 
    print(prob, team1.shape) 
    with open(out_file, "wt") as f:
        f.write("Id,Pred\n")
        for i in range(len(team1)):
            f.write("2017_%d_%d,%.2f\n" % ( team_dict[int(team1[i])], team_dict[int(team2[i])], prob[i]))
# cross-validation
train_data, team_dict = read_data_od("./data/RegularSeasonDetailedResults.csv", [2015, 2016, 2017])
# val_data, team_dict = read_data_od("./data/TourneyDetailedResults.csv", [2015, 2016, 2017], team_dict)
test_data = read_test('./data/SampleSubmission.csv', team_dict)
test = test_data.values
print(test_data.head())
pack_result(test[:, 0], test[:, 1], test[:, 3], test[:, 4], team_dict, "test_out.csv")
# print(team_dict)
# train = train_data.values
# val = val_data.values
# print(train[1, :])
