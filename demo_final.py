import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from subprocess import check_output
# import matplotlib.pyplot as plt
import tensorflow as tf
from models import *
from utils import *

  
# cross-validation
train_data, team_dict = read_data_od("./data/RegularSeasonDetailedResults.csv", [2014, 2015, 2016])
val_data, team_dict = read_data_od("./data/TourneyDetailedResults.csv", [2014, 2015, 2016], team_dict)
test_data = read_test('./data/SampleSubmission.csv', team_dict)
train = train_data.values
val = val_data.values
test = test_data.values
print(train)

params = {}
params["n_team"] = team_dict.shape[0]
params["batch_size"] = 65535 
params["gamma"] = 1e-4
params["sigma"] = 0.5

params["rank"] = 5
params["lr"] = 5e-3
params["n_epoch"] = 48000 
params["gamma"] = 1e-2
params["print"] = False 

highest_acc = 0.0
highest_params = params.copy() 

hyper_lr = [1e-1]
hyper_rank = [5]
hyper_epoch = [24000]
hyper_gamma = [1e-4]
hyper_day = [1.0]
hyper_loc = [1.0]


import itertools 
hyper_full = list(itertools.product(*[hyper_lr, hyper_rank, hyper_epoch, hyper_gamma, hyper_day, hyper_loc]))
# hyper-parameter grid search

print(params['n_team'], train.shape)
for hyper_params in hyper_full:
    params['lr'] = hyper_params[0] 
    params['rank'] = hyper_params[1] 
    params['n_epoch'] = hyper_params[2]
    params['gamma'] = hyper_params[3]
    params['day'] = hyper_params[4]
    params['loc'] = hyper_params[5]
    mf = FinalMatrixFactorization(params)
    # train(self, x1, x2, loc1, loc2, d, yo, yd):
    mf.train(train[:, 0], train[:, 1], train[:, 6]*params['loc'], train[:, 7]*params['loc'], train[:, 5]*params['day'], train[:, 3], train[:, 4])
    prediction_o, prediction_d = mf.predict(test[:, 0], test[:, 1], test[:, 6]*params['loc'], test[:, 7]*params['loc'], test[:, 5]*params['day'], test[:, 3], test[:, 4])
    print(prediction_o.shape) 
    print(prediction_d.shape) 
    pack_result(test[:, 0], test[:, 1], prediction_o, prediction_d, team_dict, "output_sample%d.csv" % params['n_epoch'])
    mf.sess.close()
    del mf
