import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
from subprocess import check_output
# import matplotlib.pyplot as plt
import tensorflow as tf
from models import *
from utils import *

  
# cross-validation
train_data, team_dict = read_data("./data/RegularSeasonDetailedResults.csv", 2016)
val_data, team_dict = read_data("./data/TourneyDetailedResults.csv", 2016, team_dict)
train = train_data.values
val = val_data.values
print(train)

params = {}
params["n_team"] = team_dict.shape[0]
params["batch_size"] = 65535 
params["gamma"] = 1e-4
params["sigma"] = 0.5

params["rank"] = 5
params["lr"] = 5e-3
params["n_epoch"] = 48000 
params["gamma"] = 1e-2
params["print"] = False 

highest_acc = 0.0
highest_params = params.copy() 

hyper_lr = [5e-2, 1e-2, 5e-3, 1e-3, 5e-4]
hyper_rank = [1, 2, 5, 10, 15]
hyper_epoch = [6000, 12000, 24000, 48000, 96000]
hyper_gamma = [1e-2, 1e-3, 1e-4, 1e-5, 1e-6]
hyper_day = [0.0, 1.0]
hyper_loc = [0.0, 1.0]


import itertools 
hyper_full = list(itertools.product(*[hyper_lr, hyper_rank, hyper_epoch, hyper_gamma, hyper_day, hyper_loc]))
# hyper-parameter grid search
for hyper_params in hyper_full:
    params['lr'] = hyper_params[0] 
    params['rank'] = hyper_params[1] 
    params['n_epoch'] = hyper_params[2]
    params['gamma'] = hyper_params[3]
    params['day'] = hyper_params[4]
    params['loc'] = hyper_params[5]
    mf = LocMatrixFactorization(params)
    mf.train(train[:, 0], train[:, 1], train[:, 5]*params['loc'], train[:, 6]*params['loc'], train[:, 4]*params['day'], train[:, 3])
    batch_acc, batch_loss = mf.validation(val[:, 0], val[:, 1], val[:, 5]*params['loc'], val[:, 6]*params['loc'], val[:, 4]*params['day'], val[:, 3])
    if batch_acc > highest_acc:
        highest_acc = batch_acc
        highest_params = params.copy()
    mf.sess.close()
    del mf

for key, value in highest_params.items():
    print(key, value, highest_acc)
